import * as  d3 from "d3";
import { IGraphConfig } from "../types/graph.data";

export const defaultGraphConfig:IGraphConfig = {
    width: 400,				//Width of the circle
    height: 400,				//Height of the circle
    margin: { top: 0, right: 0, bottom: 0, left: 0 }, //The margins of the SVG
    levels: 4,				//How many levels or inner circles should there be drawn
    maxValue: 0, 			//What is the value that the biggest circle will represent
    labelFactor: 1.40, 	//How much farther than the radius of the outer circle should the labels be placed
    wrapWidth: 100, 		    //The number of pixels after which a label needs to be given a new line
    opacityArea: 0.35, 	//The opacity of the area of the blob
    dotRadius: 4, 			//The size of the colored circles of each blog
    opacityCircles: 0.1, 	//The opacity of the circles of each blob
    strokeWidth: 1, 		//The width of the stroke around each blob
    roundStrokes: false,	//If true the area and stroke will follow a round path (cardinal-closed)
    color: d3.scaleOrdinal(d3.schemeCategory10).range(["#ffe600", "#999999"]),	//Color function,
    format: 'd',
    unit: '',
    legend: { title: 'Organization XYZ', translateX: 100, translateY: 40, offset:30,type:'vertical'},
    islegend: true,
};

export const defaultRadarStyle = {
    numRings: 4,
    axisColor: '#cdcdcd',
    ringColor: '#cdcdcd',
  };

  