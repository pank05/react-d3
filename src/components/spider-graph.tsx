import * as d3 from 'd3';
import React, { useEffect, useRef, useState } from 'react';
import { defaultGraphConfig } from '../constants/spider';
import { dataSet } from '../data/data';
import { IAxis, SpiderGraphData } from '../types/graph.data';
import SpiderGraphChart from './SpiderGraph';

const SpiderGraph  = (props:{id:string}) =>{
    const spiderChartNode = useRef(null as null | SVGSVGElement );
    let cfg = defaultGraphConfig;
    let spiderGraphChartObj : SpiderGraphChart;

    useEffect(()=>{
        const data:Array<SpiderGraphData> = (dataSet as Array<SpiderGraphData>).map((spiderData:SpiderGraphData,index:number)=>{
            spiderData.axes.map((axis:IAxis)=>{
                 axis.id = spiderData.name;
                 axis.value = parseFloat(axis.value.toFixed(1))
                 return axis;
            });
            return spiderData;
        });
        // spiderGraphChartObj = new SpiderGraphChart(cfg,'.container',data, spiderChartNode)
        // spiderGraphChartObj.init();

    },[])

    return <svg  width={cfg.width} 
            height={cfg.height} 
            viewBox={cfg.viewBox}
            className={`${props.id}-radar`} 
            ref={spiderChartNode}
            >
                <g transform={`translate(${cfg.width/2},${cfg.height/2})`}>
                    
                </g>

            </svg>;
}

export default SpiderGraph;