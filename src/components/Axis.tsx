import { select } from 'd3';
import React, { useLayoutEffect, useRef } from 'react';
import type {TickScale} from '../types/graph.data';

type RadarAxisProps = {
  scale: TickScale,
  offsetAngle: number,
  domainMax: number,
  label: string,
  color: string,
  style?: {},
  wrapWidth?:number,
};

const defaultRadarAxisStyle = {
  axisOverreach: 1.1,
  labelOverreach: 1.5,
  fontSize: 10,
  fontFamily: 'sans-serif',
  textFill: 'black',
  axisWidth: 2,
};


const wrapLabel = (labelRef:React.RefObject<SVGTextElement> , wrapWidth:number) =>{
  let textElement = select(labelRef.current);
  let words = textElement.text().split(/\s+/).reverse();
  let word:any;
  let line: string[] = [];
  let lineNumber = 0;
  let lineHeight = 1.4; // ems
  let y = textElement.attr("y");
  let x = textElement.attr("x");
  let dy = parseFloat(textElement.attr("dy"));
  let tspan = textElement.text(null).append("tspan").attr("x", x).attr("y", y).attr("dy", dy + "em");

    while (words.length > 0) {
    let word = words.pop() || '';
    line.push(word);
    tspan.text(line.join(" "));
        if ((tspan.node() as SVGTSpanElement).getComputedTextLength() > wrapWidth) {
            line.pop();
            tspan.text(line.join(" "));
            line = [word];
            lineNumber = lineNumber + 1;
            tspan = textElement.append("tspan").attr("x", x).attr("y", y).attr("dy", lineNumber * lineHeight + dy + "em").text(word);
          }
    }
}

export default function Axis(props: RadarAxisProps) {
  const {scale, offsetAngle, domainMax, label, color, style, wrapWidth} = props;
  const {
    axisOverreach,
    labelOverreach,
    fontSize,
    fontFamily,
    textFill,
    axisWidth,
  } = {...defaultRadarAxisStyle};
  const xFactor = Math.cos(offsetAngle - Math.PI / 2);
  const yFactor = Math.sin(offsetAngle - Math.PI / 2);
  const labelFieldRef = useRef<SVGTextElement>(null);

  useLayoutEffect(() => {
    if(wrapWidth){
    wrapLabel(labelFieldRef,wrapWidth);
    }
  })

  return (
    <g>
      <line
        x1={0}
        y1={0}
        x2={scale(domainMax * axisOverreach) * xFactor}
        y2={scale(domainMax * axisOverreach) * yFactor}
        stroke={color}
        strokeWidth={axisWidth}
      />
      <text
        x={scale(domainMax * labelOverreach) * xFactor}
        y={scale(domainMax * labelOverreach) * yFactor}
        fontSize={fontSize}
        fontFamily={fontFamily}
        fill={textFill}
        textAnchor={'middle'}
        ref={labelFieldRef}
        dy={'0.35em'}
      >
        {label}
      </text>
    </g>
  );
}