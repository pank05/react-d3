import * as d3 from "d3";
import { Selection } from "d3";
import { IAxis, IGraphConfig, SpiderGraphData } from "../types/graph.data";

interface SVGDatum {
    width: number;
    height: number;
}

export default class SpiderGraphChart {


    private max = Math.max;
    private sin = Math.sin;
    private cos = Math.cos;
    private HALF_PI = Math.PI / 2;

    constructor(cfg:IGraphConfig,id:String,data:Array<SpiderGraphData>,ref:React.MutableRefObject<SVGSVGElement  | null>){
        this._cfg = cfg;
        this._id= id;
        this._data = data;
        this._ref = ref; 
    }

    private _cfg:IGraphConfig;
    private _id:any;
    private _data:Array<SpiderGraphData>;
    private _ref:React.MutableRefObject<SVGSVGElement  | null>;
    private _parentSvg : d3.Selection<SVGSVGElement | null, any, null, undefined> | undefined;



    public wrap = (selection:d3.Selection<SVGTextElement, string, SVGGElement, unknown>, _width:any)  => {

        const elements:SVGTextElement[] = [...selection.nodes()];

        elements.forEach((element:SVGTextElement)=>{
            let textElement = d3.select(element);
            let words = textElement.text().split(/\s+/).reverse();
            let word:any;
            let line: string[] = [];
            let lineNumber = 0;
            let lineHeight = 1.4; // ems
            let y = textElement.attr("y");
            let x = textElement.attr("x");
            let dy = parseFloat(textElement.attr("dy"));
            let tspan = textElement.text(null).append("tspan").attr("x", x).attr("y", y).attr("dy", dy + "em");

            while (words.length > 0) {
            let word = words.pop() || '';
            line.push(word);
            tspan.text(line.join(" "));
                if ((tspan.node() as SVGTSpanElement).getComputedTextLength() > _width) {
                    line.pop();
                    tspan.text(line.join(" "));
                    line = [word];
                    lineNumber = lineNumber + 1;
                    tspan = textElement.append("tspan").attr("x", x).attr("y", y).attr("dy", lineNumber * lineHeight + dy + "em").text(word);
                  }
          }
        })
      }

    public init = () =>{

        let legendOffset:number = this._cfg.legend?.offset || 0;
        let maxValue = 0;
        let data= this._data;
        let cfg = this._cfg

        if(this._ref){
            this._parentSvg = d3.select(this._ref.current);
            // clear
            this.remove();

            // Get max axis value;
            data.forEach((spiderData:SpiderGraphData)=>{
                let maxValueInAxis = spiderData.axes.map((axis:IAxis)=>axis.value).reduce((acc,curr)=>Math.max(acc,curr),0);
                maxValue = Math.max(maxValue,maxValueInAxis);
            });

            const allAxis = data[0].axes.map((i, _j) => i.axis);	//Names of each axis
            let total = allAxis.length;				//The number of different axes
            let radius = Math.min(cfg.width / 2 - 100, cfg.height / 2); 	//Radius of the outermost circle
            let Format = d3.format(cfg.format);		 	//Formatting
            let  angleSlice = Math.PI * 2 / total;		//The width in radians of each "slice"

            //Scale for the radius
            const rScale = d3.scaleLinear()
            .range([0, radius])
            .domain([0, maxValue]);

        let chartSvg = this._parentSvg.append('svg').attr("class",'chart');
        //Append a g element
        let g = chartSvg.append("g")
            .attr("transform", "translate(" + (cfg.width / 2) + "," + (cfg.height / 2) + ")");

        //Filter for the outside glow
        let filter = g.append('defs').append('filter').attr('id', 'glow'),
            feGaussianBlur = filter.append('feGaussianBlur').attr('stdDeviation', '2.5').attr('result', 'coloredBlur'),
            feMerge = filter.append('feMerge'),
            feMergeNode_1 = feMerge.append('feMergeNode').attr('in', 'coloredBlur'),
            feMergeNode_2 = feMerge.append('feMergeNode').attr('in', 'SourceGraphic');

        //Wrapper for the grid & axes
        let axisGrid = g.append("g").attr("class", "axisWrapper");

        //Draw the background circles
        axisGrid.selectAll(".levels")
            .data(d3.range(1, (cfg.levels + 1)).reverse())
            .enter()
            .append("circle")
            .attr("class", "gridCircle")
            .attr("r", d => {
                return radius / cfg.levels * d;
            })
            .style("fill", "#ffffff")
            .style("stroke", "#CDCDCD")
            .style("fill-opacity", cfg.opacityCircles)
            .style("filter", "url(#glow)");

        //Text indicating at what % each level is
        axisGrid.selectAll(".axisLabel")
            .data(()=>{
                // console.log(d3.range(1, (cfg.levels + 1)).reverse())
                return d3.range(1, (cfg.levels + 1)).reverse();
            })
            .enter().append("text")
            .attr("class", "axisLabel")
            .attr("x", 4)
            .attr("y", d => {
                // console.log(-d * radius / cfg.levels)
                return -d * radius / cfg.levels;
            })
            .attr("dy", "0.4em")
            .style("font-size", "0.5em")
            .attr("fill", "#737373")
            .text(d => {
                console.log(maxValue,maxValue * d / cfg.levels,d)
                return Format(maxValue * d / cfg.levels)
            });

        //Create the straight lines radiating outward from the center
        var axis = axisGrid.selectAll(".axis")
            .data(allAxis)
            .enter()
            .append("g")
            .attr("class", "axis");
        //Append the lines
        axis.append("line")
            .attr("x1", 0)
            .attr("y1", 0)
            .attr("x2", (_d, i) => {
                return rScale(maxValue * 1.1) * this.cos(angleSlice * i - this.HALF_PI)
            })
            .attr("y2", (_d, i) => {
                return rScale(maxValue * 1.1) * this.sin(angleSlice * i - this.HALF_PI);
            })
            .attr("class", "line")
            .style("stroke", "#cccccc")
            .style("stroke-width", "1px");

        //Append the labels at each axis
        axis.append("text")
            .attr("class", "legend")
            .style("font-size", "0.7em")
            .attr("text-anchor", "middle")
            .attr("dy", "0.35em")
            .attr("x", (_d, i) => rScale(maxValue * cfg.labelFactor) * this.cos(angleSlice * i - this.HALF_PI))
            .attr("y", (_d, i) => rScale(maxValue * cfg.labelFactor) * this.sin(angleSlice * i - this.HALF_PI))
            .text(d => d)
            .call(this.wrap,cfg.wrapWidth)

        //The radial line function
        const radarLine = d3.lineRadial()
            .curve(d3.curveLinearClosed)
            .radius((d: any) => {
                return rScale(d.value)
            })
            .angle((_d, i) => i * angleSlice);

        if (cfg.roundStrokes) {
            radarLine.curve(d3.curveCardinalClosed)
        }

        //Create a wrapper for the blobs
        const blobWrapper = g.selectAll(".radarWrapper")
            .data(data)
            .enter().append("g")
            .attr("class", "radarWrapper");

        //Create the outlines
        blobWrapper.append("path")
            .attr("class", "radarStroke")
            .attr("d", (d: any, _i) => {
                return radarLine(d.axes);
            })
            .style("stroke-width", cfg.strokeWidth + "px")
            .style("stroke", (d, _i) => {
                return cfg.color(d.name);
            })
            .style("fill", "none")
            .style("filter", "url(#glow)");

        //Append the circles
        blobWrapper.selectAll(".radarCircle")
            .data(d => d.axes)
            .enter()
            .append("circle")
            .attr("class", "radarCircle")
            .attr("r", cfg.dotRadius)
            .attr("cx", (d, i) => rScale(d.value) * this.cos(angleSlice * i - this.HALF_PI))
            .attr("cy", (d, i) => rScale(d.value) * this.sin(angleSlice * i - this.HALF_PI))
            .style("fill", (d:any) => {
                // console.log(d)
                return cfg.color(d.id);
            })
            .style("fill-opacity", 0.8);


        //Add the text for data
        const blobCircleWrapper = g.selectAll(".radarCircleWrapper")
            .data(data)
            .enter().append("g")
            .attr("class", "radarCircleWrapper");

        blobCircleWrapper.selectAll(".radarInvisibleCircle")
            .data(d => d.axes)
            .enter().append("text")
            .attr("class", "tooltip")
            .attr('x', (d: any, i) => {
                return rScale(d.value) * this.cos(angleSlice * i - this.HALF_PI) - 10;
            })
            .attr('y', (d, i) => rScale(d.value) * this.sin(angleSlice * i - this.HALF_PI) - 10)
            .style("font-size", "12px")
            .text((d, _i) => {
                return `${d.value}`;
            })
            .attr("text-anchor", "middle")
            .attr("dy", "0.35em");

        if (cfg.islegend !== false && typeof cfg.legend === "object") {
            if(cfg.legend.type ==='vertical'){
                let stdlegendOffset = 200;
            let legendZone = this._parentSvg.append('g')
                    .attr("id","legend-zone-area")
                    .attr('transform', `translate(${stdlegendOffset-legendOffset},${cfg.legend.translateY})`)
                // legendZone.attr("");legendOffset
            let names = data.map(el => el.name);
            if (cfg.legend.title) {
                let title = legendZone.append("text")
                    .attr("class", "title")
                    // .attr('transform', `translate(${cfg.legend.translateX},${cfg.legend.translateY})`)
                    .attr("x", cfg.width - (cfg.margin.left + stdlegendOffset))
                    .attr("y", 10)
                    .attr("font-size", "14px")
                    .attr("font-weight", 700)
                    .attr("fill", "#404040")
                    .text(cfg.legend.title);
            }
            
            let legend = legendZone.append("g")
                .attr("class", "legend")
                .attr("height", 100)
                .attr("width", 200)
                // .attr('transform', `translate(${cfg.legend.translateX},${cfg.legend.translateY + 20})`);
            // Create rectangles markers
            legend.selectAll('rect')
                .data(names)
                .enter()
                .append("rect")
                .attr("x", cfg.width - (cfg.margin.left + stdlegendOffset))
                .attr("y", (_d, i) => {
                    return i * 20 +20;
                })
                .attr("width", 10)
                .attr("height", 10)
                .style("fill", (d: any, _i: number) => {
                    return cfg.color(d);
                });
            // Create labels
            legend.selectAll('text')
                .data(names)
                .enter()
                .append("text")
                .attr("x", cfg.width - (cfg.margin.left + stdlegendOffset) + 20 )
                .attr("y", (_d, i) => i * 20 + 29)
                .attr("font-size", "11px")
                .attr("font-weight", 500)
                .attr("fill", "#737373")
                .text(d => d);
            }
            if(cfg.legend.type === 'horizontal'){
               chartSvg.attr("y",55)
            let legendSvg = this._parentSvg.append("svg").attr("class",'legend-zone').attr("y",0)
            let stdlegendOffset = 200;
            let legendZone = legendSvg.append('g')
                    .attr("id","legend-zone-area")
                    .attr('transform',  "translate(" + (cfg.width / 2  - cfg.margin.right - (cfg.legend ? (cfg.legend.title.length) * 5 : 0 ) ) + "," + 20 + ")")
                // legendZone.attr("");legendOffset
            let names = data.map(el => el.name);
            if (cfg.legend.title) {
                let title = legendZone.append("text")
                    .attr("class", "title")
                    .attr("x",50)
                    .attr("y", 10)
                    .attr("font-size", "14px")
                    .attr("font-weight", 700)
                    .attr("fill", "#404040")
                    .text(cfg.legend.title);
            }
            
            let legend = legendZone.append("g")
                .attr("class", "legend")
                .attr("height", 100)
                .attr("width", 200);
                
            // Create rectangles markers
            legend.selectAll('rect')
                .data(names)
                .enter()
                .append("rect")
                .attr("x",(_d, i)=>{
                    return  i * _d.length * 10
                })
                .attr("y", 20)
                .attr("width", 10)
                .attr("height", 10)
                .style("fill", (d: any, _i: number) => {
                    return cfg.color(d);
                });
            // Create labels
            legend.selectAll('text')
                .data(names)
                .enter()
                .append("text")
                .attr("x", (_d, i) => {
                    return  i* _d.length * 10 + 20;
                } )
                .attr("y", 30)
                .attr("font-size", "11px")
                .attr("font-weight", 500)
                .attr("fill", "#737373")
                .text(d => d);
            }
            
        }
        return this._parentSvg;

        }
       
    }

    public remove(){
        if(this._parentSvg){
            this._parentSvg.selectChild().remove();
        }
        
    }
        

}