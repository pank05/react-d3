import React from 'react';
import _ from 'lodash';
import {
  convertData,
  forEachArray,
  getColorRange,
} from '../util/chart/helper';
import type {RadarPoint, RadarData} from '../types/graph.data';
import {RadarChartWrapper} from './RadarChartWrapper';

type Props = {
  data: RadarData,
  width: number,
  height: number,
  padding: number,
  domainMax: number,
  style?: {},
  onHover?: (point: RadarPoint | null) => void,
  highlighted?:RadarPoint,
  viewBox?:string,
  colorRangeFunc?:any,
};

export const RadarChart = (props: Props) => {
  const {
    data,
    width,
    height,
    padding,
    domainMax,
    style,
    onHover,
    highlighted,
    viewBox,
    colorRangeFunc,
  } = props;

  const {allPoints, scales, offsetAngles, radius ,voronoiDiagram} = convertData(
    props
  );

  const highlightedSetKey = highlighted ? highlighted.setKey : null;

  const backgroundScale = scales[data.variables[0].key];

  let colors:any = getColorRange(allPoints, colorRangeFunc);

  const [highlightedPoints, regularPoints] = _.partition(
    allPoints,
    ({setKey}) => setKey === highlightedSetKey,
  );

  return (
    <RadarChartWrapper
      variables={data.variables}
      width={width}
      height={height}
      padding={padding}
      domainMax={domainMax}
      style={style}
      onHover={onHover}
      highlighted={highlighted}
      scales={scales}
      backgroundScale={backgroundScale}
      offsetAngles={offsetAngles}
      voronoiDiagram={voronoiDiagram}
      radius={radius}
      highlightedPoint={
        highlightedPoints.length > 0 ? highlightedPoints[0] : null
      }
      regularPoints={regularPoints}
      colors={colors}
      viewBox={viewBox}
    />
  );
}