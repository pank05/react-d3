import React, { Component } from 'react';
import { defaultRadarStyle } from '../constants/spider';
import type {TickScale, RadarPoint, RadarVariable} from '../types/graph.data';
import { getHovered } from '../util/chart/helper';
import Axis from './Axis';
import RadarCircle from './RadarCircle';
import RadarRings from './RadarRings';

type Props = {
    variables: Array<RadarVariable>,
    width: number,
    height: number,
    padding: number,
    domainMax: number,
    style?: {},
    onHover?: (point: RadarPoint | null) => void,
    highlighted?: RadarPoint | null,
    scales?: {[variableKey: string]: TickScale},
    backgroundScale: TickScale ,
    offsetAngles?: {[variableKey: string]: number},
    radius: number,
    highlightedPoint: { setKey: string; points: Array<RadarPoint>; } | null,
    regularPoints: Array<{setKey: string, points: Array<RadarPoint>}>,
    colors: {[setKey: string]: string},
    voronoiDiagram?: any,
    viewBox?:string,
  };
  
  

export class RadarChartWrapper extends Component<Props,{}> {

    public static defaultProps:Props = {
        variables:[],
        height:400,
        width:400,
        padding:10,
        domainMax:3,
        highlighted:null,
        radius:2,
        highlightedPoint:null,
        regularPoints:[],
        colors:{},
        backgroundScale:{} as TickScale,
    };
    hoverMap:any = null;
    constructor(props:Props){
        super(props);
    }

    componentDidMount() {
        if (this.hoverMap) {
          this.hoverMap.addEventListener('mousemove', (event: MouseEvent) => {
            const {onHover} = this.props;
            if (!onHover) {
              return;
            }
            const {padding, height, width, radius, voronoiDiagram} = this.props;
            onHover(
              getHovered(event, height, width, padding, radius, voronoiDiagram),
            );
          });
        }
      }
      
    render() {
        const {
            width,
            height,
            padding,
            radius,
            style,
            highlighted,
            scales,
            variables,
            offsetAngles,
            domainMax,
            highlightedPoint,
            regularPoints,
            backgroundScale,
            colors,
            viewBox,
          } = this.props;

          const diameter = radius * 2;
          const {axisColor, ringColor, numRings} = {...defaultRadarStyle, ...style};
      
          const innerHeight = height - padding * 2;
          const innerWidth = width - padding * 2;
      
          const ticks = backgroundScale.ticks(numRings).slice(1);
          const tickFormat = backgroundScale.tickFormat(numRings);

        return (
            <svg width={width} height={height} viewBox={viewBox}>
            <g
              transform={`translate(${padding}, ${padding})`}
              ref={c => {
                this.hoverMap = c;
              }}
            >
              <rect
                width={diameter}
                height={diameter}
                fill={'transparent'}
                transform={
                  `translate(${(innerWidth - diameter) / 2}, ${(innerHeight -
                    diameter) /
                    2})`
                }
              />
              <g transform={`translate(${innerWidth / 2}, ${innerHeight / 2})`}>
                <RadarRings
                  ticks={ticks}
                  scale={backgroundScale}
                  color={ringColor}
                  format={tickFormat}
                  style={{ringOpacity:0}}
                />
                {variables.map(({key, label}) => {
                  return (
                    <Axis
                      key={key}
                      scale={scales? scales[key] : {} as TickScale}
                      offsetAngle={offsetAngles ? offsetAngles[key] : 0}
                      label={label}
                      domainMax={domainMax}
                      color={axisColor}
                      wrapWidth={100}
                    />
                  );
                })}
                {regularPoints.map(({setKey, points}) => {
                  return (
                    <RadarCircle
                      key={setKey}
                      points={points}
                      scales={scales || {}}
                      offsetAngles={offsetAngles || {}}
                      color={colors[setKey]}
                      isSelected={false}
                      selectedVariableKey={null}
                      style={
                        {inactiveFillOpacity:0,
                          inactiveStrokeOpacity:1}
                      }
                    />
                  );
                })}
                {
                  highlightedPoint
                    ? <RadarCircle
                      key={highlightedPoint.setKey}
                      points={highlightedPoint.points}
                      scales={scales || {}}
                      offsetAngles={offsetAngles || {}}
                      color={colors[highlightedPoint.setKey]}
                      isSelected={true}
                      selectedVariableKey={
                        highlighted ? highlighted.variableKey : null
                      }
                    />
                    : null
                }
              </g>
            </g>
          </svg>
        );
    }
}