type SpiderGraphData = {
    name:string;
    axes: Array <IAxis> 
}

export interface IAxis{
    axis:string;
    value:number;
    id?:any;
}
export interface IGraphMargin {
    top: number; 
    right: number;
    bottom: number;
    left: number;
}

export interface IGraphLegend{
    title:string;
    translateX: number;
    translateY: number;
    offset?:number;
    type?: 'horizontal' | 'vertical';
}

export interface IGraphConfig {
    viewBox?: string | undefined;
    width: number;		    //Width of the Graph
    height: number; 	    //Height of the Graph
    margin: IGraphMargin;   //The margins of the SVG
    levels: number;			//How many levels or inner circles should there be drawn
    maxValue: number, 		//What is the value that the biggest circle will represent
    labelFactor: number, 	//How much farther than the radius of the outer circle should the labels be placed
    wrapWidth: number, 		//The number of pixels after which a label needs to be given a new line
    opacityArea: number, 	//The opacity of the area of the blob
    dotRadius: number, 		//The size of the colored circles of each blog
    opacityCircles: number, //The opacity of the circles of each blob
    strokeWidth: number, 	//The width of the stroke around each blob
    roundStrokes: boolean,	//If true the area and stroke will follow a round path (cardinal-closed)
    color: any,	            //Color function,
    format: string,         // Formate applied
    unit: string,           // unit of graph
    legend?:IGraphLegend,
    islegend:boolean,
}

export type TickScale = {
    (d: number): number,
    ticks(count: number): Array<number>,
    tickFormat(count: number, fmt?:string): (val: number) => string,
  };
  
  export type RadarVariable = {key: string, label: string};
  
  type RadarDataSet = {
    key: string,
    label: string,
    values: {[variableKey: string]: number},
  };
  
  export type RadarData = {
    variables: Array<RadarVariable>,
    sets: Array<RadarDataSet>,
  };
  
  export type RadarPoint = {
    x: number,
    y: number,
    value: number | any,
    setKey: string,
    variableKey: string,
    key: string,
  };