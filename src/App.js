import {RadarChart} from './components/RadarChart';
import "./App.css";
import { scaleOrdinal } from 'd3';
import {dataSet} from './data/data';
import {transformToSpiderData} from './util/chart/helper'
import SpiderGraph from './components/spider-graph';
function App() {
  return (
    <div className="App">
      <div style={{padding:'15px',fontSize:'18px', fontWeight:'bold'}}>Organization XYZ (Details)</div>
      <div style={{margin:'10px'}} className="border-all">
      <RadarChart
        width={400}
        height={400}
        padding={70}
        domainMax={5}
        highlighted={null}
        onHover={(point) => {
          if (point) {
            console.log('hovered over a data point');
          } else {
            console.log('not over anything');
          }
        }}
        spiderData={dataSet}
        data={transformToSpiderData(dataSet)}
        viewBox={"0 0 400 480"}
        colorRangeFunc= {scaleOrdinal().domain(["allocated_budget","actual_spending"]).range(["#ffe600", "#999999"])}
/>
      </div>
  <div >
  
  </div>
    </div>
  );
}

export default App;

{/* <Radar
width={400}
height={400}
padding={70}
domainMax={5}
highlighted={null}
onHover={(point) => {
  if (point) {
    console.log('hovered over a data point');
  } else {
    console.log('not over anything');
  }
}}
spiderData={dataSet}
data={transformToSpiderData(dataSet)}
viewBox={"0 0 400 480"}
/> */}