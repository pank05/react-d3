
import { schemeCategory10 } from 'd3';
import {scaleLinear} from 'd3-scale';
import { voronoi } from 'd3-voronoi';

import { flatMapDeep, forEach, indexOf, sortBy } from 'lodash';
import type {TickScale, RadarPoint, RadarData, RadarVariable, SpiderGraphData, IAxis, RadarDataSet} from '../../types/graph.data';

export const  getHovered = (
    event: MouseEvent,
    height: number,
    width: number,
    padding: number,
    radius: number,
    voronoiDiagram: any,
  ):any => {
    const innerHeight = height - padding * 2;
    const innerWidth = width - padding * 2;
    const diameter = radius * 2;
  
    let {offsetX: clientX, offsetY: clientY} = event;
    clientX -= padding;
    clientY -= padding;
    clientX -= (innerWidth - diameter) / 2;
    clientY -= (innerHeight - diameter) / 2;
  
    const site = voronoiDiagram.find(clientX, clientY, radius / 2);
    if (!site) {
      return null;
    }
  
    const {data} = site;
    return data;
  }


export function flatMapDeepArray<T, R>(
  collection: Array<T>,
  fn: (d: T) => Array<R>,
): Array<R> {
  return flatMapDeep(collection, fn);
}
export function forEachArray<T>(
  collection: Array<T>,
  fn: (item: T, idx: number) => void,
): void {
forEach(collection, fn);
}

export function radiusScales(
  variables: Array<RadarVariable>,
  domainMax: number,
  radius: number,
): {[variableKey: string]: TickScale} {
  let res:any = {};
  forEach(variables, ({key}) => {
    const scale = scaleLinear().domain([0, domainMax]).range([0, radius]);
    res[key] = scale;
  });
  return res;
}

export const transformToSpiderData = (data:SpiderGraphData[]):RadarData => {
  const transformSets:Array<RadarDataSet> = data.map(({axes,name}) =>{
    let tmp:any ={}
    tmp.key =  generateStringKey(name);
    tmp.label = name;
    tmp.values = axes.map((val)=>{
        let id = generateStringKey(val.axis);
        return {...val,id}
    })
    return tmp as RadarDataSet;
  });
  let Axis = data[0].axes;
  const allAxis = Axis.map((i:any,key:number) =>{
    return {key: generateStringKey(i.axis) ,label:i.axis}
  });

  return {
    sets:transformSets,
    variables:allAxis
  }  as RadarData;
}

export const convertData = (props:any) => {
  console.log(props)
  const {data, width, height, padding, domainMax,spiderData} = props;

  let angleSliceRadians = Math.PI * 2 / data.variables.length;

  const innerHeight = height - padding * 2;
  const innerWidth = width - padding * 2;

  const radius = Math.min(innerWidth / 2, innerHeight / 2);
  let scales:any; 
  let offsetAngles:any = {};
  let allPoints:any;
  
  scales = radiusScales(data.variables, domainMax, radius);

  angleSliceRadians = Math.PI * 2 / data.variables.length;
  forEachArray(data.variables, ({key}: any, i: number) => {
    offsetAngles[key] = angleSliceRadians * i;
  });

  allPoints = radarPoints(data, scales, offsetAngles);
  const flatPointList:any = flatMapDeepArray(allPoints, ({points}:any) => {
    return points;
  });

  const voronoiDiagram:any = voronoi()
    .x((d: any) => d.x + radius)
    .y((d: any) => d.y + radius)
    .size([radius * 2, radius * 2])(flatPointList);

  return {allPoints, scales, offsetAngles, voronoiDiagram, radius};
}

// Return a key for the string
export const generateStringKey = (str:string | undefined):string => {
  return (str || '').toLowerCase().replace(/[^A-Z0-9]/ig, "_");
}

export function radarPoints(data:RadarData, scales: {[variableKey: string]: TickScale},
  offsetAngles: {[variableKey: string]: number}){

   const allVariableKeys = data.variables.map((variable:any) => variable.key);

     return data.sets.map(({key, values}) => {
      const points:any = [];

      forEach(values as unknown as IAxis, (value) => {
        const scale = scales[value.id];
        const offsetAngle = offsetAngles[value.id];

        if (scale === undefined || offsetAngle === undefined) {
          return;
        }
        const x = scale(value.value) * Math.cos(offsetAngle - Math.PI / 2);
        const y = scale(value.value) * Math.sin(offsetAngle - Math.PI / 2);

        const point = {
          x,
          y,
          value,
          setKey: key,
          variableKey:value.id,
          key: `${key}--${value.id}`,
        };
        points.push(point);
      });
      const sortedPoints = sortBy(points, point => {
        const pointVariableKey = point.variableKey;
        return indexOf(allVariableKeys, pointVariableKey);
      });

  
      return {setKey: key, points: sortedPoints};

  })
}

export const getColorRange= (allPoints:any,scaleFunc?:any) => {
  let colors:any = {};
  forEachArray(allPoints, ({setKey}: any, idx: number) => {
    colors[setKey] = scaleFunc ? scaleFunc(setKey) : schemeCategory10[idx];
  });
  return colors;
}